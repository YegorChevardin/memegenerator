import { render } from 'preact'
import './assets/index.css'
import App from "./App"

const page = (
    <App/>
)

render(page, document.getElementById('root'))
