export default function Image(props) {
    return (
        <div className="container">
            <div className="d-flex justify-content-center align-items-center">
                <div className="card text-bg-dark w-50">
                    <img src={props.url} alt={props.name} className="card-img"/>
                    <div className="card-img-overlay d-flex justify-content-between align-items-center flex-column">
                        <p className="meme-text">{props.leftText}</p>
                        <p className="meme-text">{props.rightText}</p>
                    </div>
                </div>
            </div>
        </div>
    )
}