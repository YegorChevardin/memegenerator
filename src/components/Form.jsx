import Image from "./Image"
import React from "react";

export default function Form() {
    const [formData, setLeftAndRightText] = React.useState(
        {
            leftText: "",
            rightText: ""
        }
    )
    const [randomMemeElement, setRandomMemeElement] = React.useState(
        (
            <div className="d-flex justify-content-center align-items-center">
                <h1>No image selected!</h1>
            </div>
        )
    )
    const [memesData, setMemesData] = React.useState([]);

    React.useEffect(() => {
        fetch("https://api.imgflip.com/get_memes")
            .then(res => res.json())
            .then(ApiData => {
                setMemesData(ApiData.data.memes);
            })
    }, [])

    function handleChange(event) {
        setLeftAndRightText(prevFormData => {
            return {
                ...prevFormData,
                [event.target.name]: event.target.value
            }
        })
    }

    function changeImage(event) {
        event.preventDefault()

        let leftText = formData.leftText
        let rightText = formData.rightText
        const randomMeme = memesData[parseInt(Math.random() * memesData.length)]
        setRandomMemeElement(
            <Image id={randomMeme.id} {...randomMeme} leftText={leftText} rightText={rightText}/>
        )
    }

    return (
        <div>
            <div className="container mt-5 mb-4">
                <form onSubmit={changeImage}>
                    <div className="row align-items-center justify-content-center">
                        <div className="col-md mt-1 text-center">
                            <input onChange={handleChange} value={formData.leftText} name="leftText" type="text" className="form-control" id="input1" placeholder="Enter 1st phrase"/>
                        </div>
                        <div className="col-md mt-1 text-center">
                            <input onChange={handleChange} value={formData.rightText} name="rightText" type="text" className="form-control" id="input2" placeholder="Enter 2nd phrase"/>
                        </div>
                    </div>
                    <div className="row align-items-center justify-content-center mt-3">
                        <div className="col-md-12 text-center">
                            <button type="submit" className="btn w-100 bg-gradient-slided text-light mb-3">
                                Get a new meme image 🖼
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            {randomMemeElement}
        </div>
    )
}