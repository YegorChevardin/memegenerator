import trollFace from "/src/assets/images/Troll Face.png"

export default function Header() {
    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-gradient">
            <div className="container">
                <a className="navbar-brand" href="#">
                    <img className="me-1" src={trollFace} alt="Logo" width="30" height="24"/>
                    MemeGenerator
                </a>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
                        <li className="nav-item">
                            <a className="nav-link active" aria-current="page" href="#">
                                React Course - Project 3
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    );
}